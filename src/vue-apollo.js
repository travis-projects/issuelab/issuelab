import Vue from 'vue'
import VueApollo from 'vue-apollo'
import { createApolloClient, restartWebsockets } from 'vue-cli-plugin-apollo/graphql-client'
import GET_USER_QUERY from './graphql/User/GetUser.gql'

// Install the vue plugin
Vue.use(VueApollo)

// Name of the localStorage item
const AUTH_TOKEN = 'apollo-token'
const AUTH_USER = 'apollo-user'

// Http endpoint
const httpEndpoint = process.env.VUE_APP_GRAPHQL_HTTP || 'http://localhost:4000'

// Config
const defaultOptions = {
  // You can use `https` for secure connection (recommended in production)
  httpEndpoint,
  // You can use `wss` for secure connection (recommended in production)
  // Use `null` to disable subscriptions
  wsEndpoint: process.env.VUE_APP_GRAPHQL_WS || 'ws://localhost:4000',
  // LocalStorage token
  tokenName: AUTH_TOKEN,
  // Enable Automatic Query persisting with Apollo Engine
  persisting: false,
  // Use websockets for everything (no HTTP)
  // You need to pass a `wsEndpoint` for this to work
  websocketsOnly: false,
  // Is being rendered on the server?
  ssr: false

  // Override default http link
  // link: myLink

  // Override default cache
  // cache: myCache

  // Override the way the Authorization header is set
  // getAuth: (tokenName) => ...

  // Additional ApolloClient options
  // apollo: { ... }

  // Client local data (see apollo-link-state)
  // clientState: { resolvers: { ... }, defaults: { ... } }
}

// Create apollo client
export const { apolloClient, wsClient } = createApolloClient({
  ...defaultOptions
})
apolloClient.wsClient = wsClient

// Call this in the Vue app file
export function createProvider () {
  // Create vue apollo provider
  const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
    defaultOptions: {
      $query: {
        // fetchPolicy: 'cache-and-network',
      }
    },
    errorHandler (error) {
      // eslint-disable-next-line no-console
      console.log('%cError', 'background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;', error.message)
    }
  })

  return apolloProvider
}

// Create Global Auth Plugin functions
export const authFunctions = {
  isAuthenticated () {
    if (localStorage.getItem('apollo-token')) return true
    else return false
  },
  async isAuthorized () {
    if (this.isAuthenticated()) {
      const { data: { me } } = await apolloClient.query({ query: GET_USER_QUERY })
      return me.authenticated
    }
  },
  getToken () {
    return localStorage.getItem('apollo-token')
  },
  getUserId () {
    return localStorage.getItem('apollo-user')
  },
  async onLogout () {
    localStorage.removeItem(AUTH_TOKEN)
    localStorage.removeItem(AUTH_USER)
    if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient)
    try {
      await apolloClient.resetStore()
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('%cError on cache reset (logout)', 'color: orange;', e.message)
    }
  },
  async onLogin (token, user) {
    localStorage.setItem(AUTH_TOKEN, token)
    localStorage.setItem(AUTH_USER, user)
    if (apolloClient.wsClient) await restartWebsockets(apolloClient.wsClient)
    try {
      await apolloClient.resetStore()
      await apolloClient.query({
        query: GET_USER_QUERY
      })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('%cError on cache reset (login)', 'color: orange;', e.message)
    }
  }
}

export const auth = {
  install (Vue, options) {
    Vue.prototype.$auth = authFunctions
  }
}
