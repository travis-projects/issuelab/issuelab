import Vue from 'vue'
import Router from 'vue-router'
import { authFunctions } from './vue-apollo'

// Import Views
import Home from './views/Home'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'auth',
      component: () => import(/* webpackChunkName: "auth" */ './views/Login')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About')
    },
    {
      path: '/authenticate',
      name: 'authenticate',
      component: () => import(/* webpackChunkName: "authenticate" */ './views/Authenticate'),
      meta: {
        requiresAuthentication: true
      }
    },
    {
      path: '/authenticate/redirect',
      name: 'redirect',
      component: () => import(/* webpackChunkName: "auth" */ './views/Redirect'),
      meta: {
        requiresAuthentication: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import(/* webpackChunkName: "dashboard" */ './views/Dashboard'),
      meta: {
        requiresAuthentication: true
      }
    },
    {
      path: '/create',
      name: 'create',
      component: () => import(/* webpackChunkName: "create" */ './views/Create'),
      meta: {
        requiresAuthentication: true
      }
    },
    {
      path: '/form/:slug',
      name: 'form',
      component: () => import(/* webpackChunkName: "auth" */ './views/Form'),
      props: route => ({ slug: route.params.slug })
    }
  ]
})

router.beforeEach((to, from, next) => {
  const authenticated = authFunctions.isAuthenticated()
  const authorized = authFunctions.isAuthorized()

  const requiresAuthentication = to.matched.some((route) => route.meta.requiresAuthentication)
  const requiresAuthorization = to.matched.some((route) => route.meta.requiresAuthorization)
  if (requiresAuthentication && !authenticated) next({ name: 'auth' })
  else if (requiresAuthentication && authenticated) {
    if (requiresAuthorization && !authorized) next({ name: 'authenticate' })
    else if (requiresAuthorization && authorized) next()
    else next()
  } else next()
})

export default router
