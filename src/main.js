import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import { createProvider, auth } from './vue-apollo'
import Buefy from 'buefy'

import './assets/scss/app.scss'

Vue.use(Buefy, {
  defaultIconPack: 'fas'
})
Vue.use(auth)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  provide: createProvider().provide(),
  render: h => h(App)
}).$mount('#app')
